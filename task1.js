//task1
//задание 1

function twoDimensional(mas, n) {
    let mas1 = [];
    let m = mas.length;
    for(let i=0; i<m/n; i++){
        let minmas = [];
        minmas = mas.splice(0,n);
        mas1.push(minmas);
    }
    console.log(mas1);
}
twoDimensional([0, 1, 2, 3, 4, 5, 6], 2)

//задание 2

function findLargest(...mas){
    let maxmas = [];
    for (let i = 0; i<mas.length; i++){
        let max = Math.max(...mas[i]);
        maxmas.push(max);
    }
    console.log(maxmas)
}
findLargest([1,2,4], [2,4,3], [43,2,1])

//задание 3

function findEl(mas, func) {
    const newArr = mas.find(function(el){return func(el)});
    return newArr;
}
console.log(findEl([1, 5, 10, 19, 23, 30], function(num) { return num % 2 === 0; }));

//задание 4
function letterAlphabet(str){
    let arr = str.split('').sort()
    for (let i=1; i<arr.length; i++){
        let curr = arr[i].charCodeAt();
        let prev = arr[i-1].charCodeAt();
        if((curr - prev)!= 1){
            let answer = [];
            let num = curr - prev;
            for(let i = 1 ; i < num ; i ++){
                answer.push(String.fromCharCode(prev + i));
            }
            for (let i =0; i<answer.length;i++)
            console.log(answer[i]);
        }
    }
}
letterAlphabet("abceg");
